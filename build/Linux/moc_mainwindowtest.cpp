/****************************************************************************
** Meta object code from reading C++ file 'mainwindowtest.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/GUI/mainwindowtest.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindowtest.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindowTest_t {
    QByteArrayData data[7];
    char stringdata0[127];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindowTest_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindowTest_t qt_meta_stringdata_MainWindowTest = {
    {
QT_MOC_LITERAL(0, 0, 14), // "MainWindowTest"
QT_MOC_LITERAL(1, 15, 27), // "_slotShowSettingApplication"
QT_MOC_LITERAL(2, 43, 0), // ""
QT_MOC_LITERAL(3, 44, 20), // "_slotShowSettingTest"
QT_MOC_LITERAL(4, 65, 21), // "_slotShowTableReports"
QT_MOC_LITERAL(5, 87, 25), // "_slotShowAboutApplication"
QT_MOC_LITERAL(6, 113, 13) // "_slotShowHelp"

    },
    "MainWindowTest\0_slotShowSettingApplication\0"
    "\0_slotShowSettingTest\0_slotShowTableReports\0"
    "_slotShowAboutApplication\0_slotShowHelp"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindowTest[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   39,    2, 0x08 /* Private */,
       3,    0,   40,    2, 0x08 /* Private */,
       4,    0,   41,    2, 0x08 /* Private */,
       5,    0,   42,    2, 0x08 /* Private */,
       6,    0,   43,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void MainWindowTest::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindowTest *_t = static_cast<MainWindowTest *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->_slotShowSettingApplication(); break;
        case 1: _t->_slotShowSettingTest(); break;
        case 2: _t->_slotShowTableReports(); break;
        case 3: _t->_slotShowAboutApplication(); break;
        case 4: _t->_slotShowHelp(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObject MainWindowTest::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindowTest.data,
      qt_meta_data_MainWindowTest,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindowTest::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindowTest::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindowTest.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindowTest::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 5)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 5;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
