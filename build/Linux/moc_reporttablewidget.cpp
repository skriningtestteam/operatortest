/****************************************************************************
** Meta object code from reading C++ file 'reporttablewidget.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../src/GUI/MainMenuWidget/reporttablewidget.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'reporttablewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ReportTableWidget_t {
    QByteArrayData data[6];
    char stringdata0[149];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ReportTableWidget_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ReportTableWidget_t qt_meta_stringdata_ReportTableWidget = {
    {
QT_MOC_LITERAL(0, 0, 17), // "ReportTableWidget"
QT_MOC_LITERAL(1, 18, 28), // "_slotHandleButtonApplyFilter"
QT_MOC_LITERAL(2, 47, 0), // ""
QT_MOC_LITERAL(3, 48, 32), // "_slotHandleButtonSelectFirstDate"
QT_MOC_LITERAL(4, 81, 31), // "_slotHandleButtonSelectLastDate"
QT_MOC_LITERAL(5, 113, 35) // "_slotHandleSignalDatePeriodHa..."

    },
    "ReportTableWidget\0_slotHandleButtonApplyFilter\0"
    "\0_slotHandleButtonSelectFirstDate\0"
    "_slotHandleButtonSelectLastDate\0"
    "_slotHandleSignalDatePeriodHaveDate"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ReportTableWidget[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    2, 0x08 /* Private */,
       3,    0,   35,    2, 0x08 /* Private */,
       4,    0,   36,    2, 0x08 /* Private */,
       5,    2,   37,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QDate, QMetaType::Bool,    2,    2,

       0        // eod
};

void ReportTableWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ReportTableWidget *_t = static_cast<ReportTableWidget *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->_slotHandleButtonApplyFilter(); break;
        case 1: _t->_slotHandleButtonSelectFirstDate(); break;
        case 2: _t->_slotHandleButtonSelectLastDate(); break;
        case 3: _t->_slotHandleSignalDatePeriodHaveDate((*reinterpret_cast< QDate(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        default: ;
        }
    }
}

const QMetaObject ReportTableWidget::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_ReportTableWidget.data,
      qt_meta_data_ReportTableWidget,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ReportTableWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ReportTableWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ReportTableWidget.stringdata0))
        return static_cast<void*>(this);
    return QWidget::qt_metacast(_clname);
}

int ReportTableWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
