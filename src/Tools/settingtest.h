#ifndef SETTINGTEST_H
#define SETTINGTEST_H

#include <QtCore>
#include <QDateTime>

class SettingTest
{
public:
    SettingTest();
    ~SettingTest();

public:

    QString Fullname() const;
    void SetFullname(const QString &Fullname);

    QString Birthday() const;
    void SetBirthday(const QString &Birthday);

    QDateTime DateTimeTest() const;
    void SetDatetimeTest(const QDateTime &DateTimeTest);

    QString TypeTest() const;
    void SetTypeTest(const QString &TypeTest);

    QString OrganizationTest() const;
    void SetOrganization(const QString &OrganizationTest);

    QString OperatorTest() const;
    void SetOperatorTest(const QString &OperatorTest);

public slots:

private:

private slots:

private:
    QString _fullname;
    QString _birthday;
    QDateTime _datetimeTest;
    QString _typeTest;
    QString _organizationTest;
    QString _operatorTest;
};

#endif // SETTINGTEST_H
