#ifndef MAINCONTOLLER_H
#define MAINCONTOLLER_H


#include "settingapplication.h"

class MainContoller
{
public:
    MainContoller();
    ~MainContoller();

 public:
    SettingApplication* GetSettingApplication();

 public slots:

 private:

 private slots:

 private:
    SettingApplication * _settingApplication;
};

#endif // MAINCONTOLLER_H
