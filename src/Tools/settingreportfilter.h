#ifndef SETTINGREPORTFILTER_H
#define SETTINGREPORTFILTER_H

#include <QDate>
#include <QTime>


class SettingReportFilter
{
public:
    SettingReportFilter();
    ~SettingReportFilter();

public:

    QDate FirstDate() const;
    void SetFirstDate(const QDate &FirstDate);

    QDate LastDate() const;
    void SetLastDate(const QDate &LastDate);

    QTime FirstTime() const;
    void SetFirstTime(const QTime &FirstTime);

    QTime LastTime() const;
    void SetLastTime(const QTime &LastTime);

    bool TestTypes1() const;
    void SetTestTypes1(bool TestTypes1);

    bool TestTypes2() const;
    void SetTestTypes2(bool TestTypes2);

    bool TestTypes3() const;
    void SetTestTypes3(bool TestTypes3);

private:

private:
    QDate _firstDate;
    QDate _lastDate;
    QTime _firstTime;
    QTime _lastTime;
    bool _testTypes1;
    bool _testTypes2;
    bool _testTypes3;
};

#endif // SETTINGREPORTFILTER_H
