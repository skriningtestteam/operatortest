#include "settingapplication.h"

SettingApplication::SettingApplication(QString filename):
    _settingFilename(filename)
{
    this->_ipaddres = "127.0.0.1";
    this->_reportDirectory = "";
    this->_LoadSetting();
}

SettingApplication::~SettingApplication()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
QString SettingApplication::GetIPAddres()
{
    return this->_ipaddres;
}

void SettingApplication::SetIPAddres(QString ipaddres)
{
    if (this->_ValidateIPAddres())
    {
        this->_ipaddres = ipaddres;
        this->_SaveSetting();
    }
    else
    {
        throw TestException(13, "Bad Format IP Addres");
    }
}

QString SettingApplication::GetReportDirectory()
{
    return this->_reportDirectory;
}

void SettingApplication::SetReportDirectory(QString dirname)
{
    this->_reportDirectory = dirname;
    this->_SaveSetting();
}
/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void SettingApplication::_SaveSetting()
{
    QSettings settings(this->_settingFilename, QSettings::IniFormat);
    settings.beginGroup("ApplicationSetting");
    settings.setValue("IP", this->_ipaddres);
    settings.setValue("ReportDirectory", this->_reportDirectory);
    settings.endGroup();
}

void SettingApplication::_LoadSetting()
{
    if (QFile::exists(this->_settingFilename))
    {
        QSettings settings(this->_settingFilename, QSettings::IniFormat);
        settings.beginGroup("ApplicationSetting");
        this->_ipaddres = settings.value("IP", "000.000.000.000").toString();
        this->_reportDirectory = settings.value("ReportDirectory", "").toString();
        settings.endGroup();
    }
}

bool SettingApplication::_ValidateIPAddres()
{
    return true;
}
/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
