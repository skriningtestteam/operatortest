#include "settingreportfilter.h"

SettingReportFilter::SettingReportFilter()
{
    this->_firstDate = QDate(2000, 1, 1);
    this->_lastDate = QDate::currentDate();
    this->_firstTime = QTime(0, 0, 0);
    this->_lastTime = QTime(23, 59, 59);

    this->_testTypes1 = true;
    this->_testTypes2 = true;
    this->_testTypes3 = true;
}

SettingReportFilter::~SettingReportFilter()
{

}


/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
QDate SettingReportFilter::FirstDate() const
{
    return _firstDate;
}

void SettingReportFilter::SetFirstDate(const QDate &firstDate)
{
    _firstDate = firstDate;
}

QDate SettingReportFilter::LastDate() const
{
    return _lastDate;
}

void SettingReportFilter::SetLastDate(const QDate &lastDate)
{
    _lastDate = lastDate;
}

QTime SettingReportFilter::FirstTime() const
{
    return _firstTime;
}

void SettingReportFilter::SetFirstTime(const QTime &firstTime)
{
    _firstTime = firstTime;
}

QTime SettingReportFilter::LastTime() const
{
    return _lastTime;
}

void SettingReportFilter::SetLastTime(const QTime &lastTime)
{
    _lastTime = lastTime;
}

bool SettingReportFilter::TestTypes1() const
{
    return _testTypes1;
}

void SettingReportFilter::SetTestTypes1(bool testTypes1)
{
    _testTypes1 = testTypes1;
}

bool SettingReportFilter::TestTypes2() const
{
    return _testTypes2;
}

void SettingReportFilter::SetTestTypes2(bool testTypes2)
{
    _testTypes2 = testTypes2;
}

bool SettingReportFilter::TestTypes3() const
{
    return _testTypes3;
}

void SettingReportFilter::SetTestTypes3(bool testTypes3)
{
    _testTypes3 = testTypes3;
}
/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
