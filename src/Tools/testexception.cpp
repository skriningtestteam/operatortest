#include "testexception.h"

TestException::TestException(uint code, QString msg):
    _codeError(code),
    _msgError(msg)
{

}

TestException::~TestException()
{

}

uint TestException::GetCodeError()
{
    return this->_codeError;
}

QString TestException::GetMessageError()
{
    return this->_msgError;
}
