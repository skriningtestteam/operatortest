#include "settingtest.h"

SettingTest::SettingTest()
{

}

SettingTest::~SettingTest()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
QString SettingTest::Fullname() const
{
    return _fullname;
}

void SettingTest::SetFullname(const QString &fullname)
{
    _fullname = fullname;
}

QString SettingTest::Birthday() const
{
    return _birthday;
}

void SettingTest::SetBirthday(const QString &birthday)
{
    _birthday = birthday;
}

QDateTime SettingTest::DateTimeTest() const
{
    return _datetimeTest;
}

void SettingTest::SetDatetimeTest(const QDateTime &datetimeTest)
{
    _datetimeTest = datetimeTest;
}

QString SettingTest::TypeTest() const
{
    return _typeTest;
}

void SettingTest::SetTypeTest(const QString &typeTest)
{
    _typeTest = typeTest;
}

QString SettingTest::OrganizationTest() const
{
    return _organizationTest;
}

void SettingTest::SetOrganization(const QString &organization)
{
    _organizationTest = organization;
}

QString SettingTest::OperatorTest() const
{
    return _operatorTest;
}

void SettingTest::SetOperatorTest(const QString &operatorTest)
{
    _operatorTest = operatorTest;
}

/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
