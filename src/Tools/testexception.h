#ifndef TESTEXCEPTION_H
#define TESTEXCEPTION_H

#include <QException>

class TestException : public QException
{
public:
    TestException(uint, QString);
    ~TestException();

    uint GetCodeError();
    QString GetMessageError();

private:
    uint _codeError;
    QString _msgError;
};

#endif // TESTEXCEPTION_H
