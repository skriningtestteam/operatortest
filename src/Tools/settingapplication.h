#ifndef SETTINGAPPLICATION_H
#define SETTINGAPPLICATION_H

#include <QtCore>
#include <QSettings>
#include <QFile>

#include "testexception.h"

class SettingApplication
{
public:
    SettingApplication(QString);
    ~SettingApplication();

public:
    QString GetIPAddres();
    QString GetReportDirectory();
    void SetIPAddres(QString);
    void SetReportDirectory(QString);

public slots:

private:
    void _LoadSetting();
    void _SaveSetting();

    bool _ValidateIPAddres();

private slots:

private:
    QString _settingFilename;
    QString _ipaddres;
    QString _reportDirectory;

};

#endif // SETTINGAPPLICATION_H
