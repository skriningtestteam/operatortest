#include <QApplication>

#include "GUI/mainwindowtest.h"
#include "Tools/maincontoller.h"

int main(int argc, char *argv[])
{
    MainContoller* _controller = new MainContoller();
    QApplication a(argc, argv);
    MainWindowTest w(_controller);
    w.show();

    return a.exec();
}
