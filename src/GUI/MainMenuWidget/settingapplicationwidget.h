#ifndef SETTINGAPPLICATIONWIDGET_H
#define SETTINGAPPLICATIONWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QFileDialog>
#include <QErrorMessage>

#include "../../Tools/settingapplication.h"

class SettingApplicationWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingApplicationWidget(SettingApplication*);

signals:

public:

public slots:

private:
    void _InitWidget();

private slots:
    void _slotHandlerButtonOK();
    void _slotButtonSelectReportDirectory();

private:
    SettingApplication* _settingApplication;

    QLabel* _lblIPAddres;
    QLabel* _lblReportDirectory;
    QLineEdit* _ledIPAddres;
    QLineEdit* _ledReportDirectory;
    QPushButton* _btnOK;
    QPushButton* _btnSelectReportDirectory;

};

#endif // SETTINGAPPLICATIONWIDGET_H
