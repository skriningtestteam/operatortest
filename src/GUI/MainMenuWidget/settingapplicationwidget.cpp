#include "settingapplicationwidget.h"

SettingApplicationWidget::SettingApplicationWidget(SettingApplication* sett):
    _settingApplication(sett)
{
    this->setFixedSize(600, 200);
    this->setWindowTitle(tr("Настройки приложения"));

    this->_InitWidget();
}


/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void SettingApplicationWidget::_InitWidget()
{
    this->_lblIPAddres = new QLabel(this);
    this->_lblIPAddres->setGeometry(20, 10, 280, 20);
    this->_lblIPAddres->setText(tr("IP-адрес сенсорного устройства"));
    this->_ledIPAddres = new QLineEdit(this);
    this->_ledIPAddres->setGeometry(310, 10, 150, 20);
    this->_ledIPAddres->setText(this->_settingApplication->GetIPAddres());

    this->_btnOK = new QPushButton(this);
    this->_btnOK->setGeometry(500, 20, 80, 40);
    this->_btnOK->setText("OK");
    connect(this->_btnOK, SIGNAL(clicked()), this, SLOT(_slotHandlerButtonOK()));

    this->_lblReportDirectory = new QLabel(this);
    this->_lblReportDirectory->setGeometry(20, 120, 220, 20);
    this->_lblReportDirectory->setText(tr("Каталог для отчетов"));
    this->_ledReportDirectory = new QLineEdit(this);
    this->_ledReportDirectory->setGeometry(250, 120, 250, 20);
    this->_ledReportDirectory->setText(this->_settingApplication->GetReportDirectory());
    this->_btnSelectReportDirectory = new QPushButton(this);
    this->_btnSelectReportDirectory->setGeometry(520, 120, 60, 40);
    this->_btnSelectReportDirectory->setText(tr("Каталог"));
    connect(this->_btnSelectReportDirectory, SIGNAL(clicked()), this, SLOT(_slotButtonSelectReportDirectory()));
}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */
void SettingApplicationWidget::_slotHandlerButtonOK()
{
    try
    {
        this->_settingApplication->SetIPAddres(this->_ledIPAddres->text());

        this->_settingApplication->SetReportDirectory(this->_ledReportDirectory->text());
        this->close();
    }
    catch (TestException err)
    {
        QErrorMessage errorMessage;
        errorMessage.showMessage(err.GetMessageError());
        errorMessage.exec();
    }

}

void SettingApplicationWidget::_slotButtonSelectReportDirectory()
{
    QString _reportDirectory = QFileDialog::getExistingDirectory(0, tr("Выберите каталог для хранения отчетов"),
                                                                 QDir::currentPath());
    this->_ledReportDirectory->setText(_reportDirectory);
}
/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
