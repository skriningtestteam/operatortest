#include "settingtestwidget.h"

SettingTestWidget::SettingTestWidget()
{
    this->setWindowTitle("Настройка тестирования");
    this->setFixedSize(700, 240);

    this->_InitWidget();
}

SettingTestWidget::~SettingTestWidget()
{
    delete this->_grbSetting;
    delete this->_grbCommand;
}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
void SettingTestWidget::ShowWidget()
{
    this->_ledTestDateTime->setText(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
    this->show();
}

void SettingTestWidget::SetListTestType(QStringList testtypes)
{
    this->_SetComboboxListItems(this->_cmbTestType, testtypes);
}

void SettingTestWidget::SetListTestOrganization(QStringList testorganization)
{
    this->_SetComboboxListItems(this->_cmbOrganization, testorganization);
}

void SettingTestWidget::SetListTestOperator(QStringList testoperator)
{
    this->_SetComboboxListItems(this->_cmbTestOperator, testoperator);
}
/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void SettingTestWidget::_InitWidget()
{
    this->_grbSetting = new QGroupBox(this);
    this->_grbSetting->setGeometry(10, 10, 550, 220);

    this->_lblFullname = new QLabel(this->_grbSetting);
    this->_lblFullname->setGeometry(10, 30, 200, 20);
    this->_lblFullname->setText(tr("Фамилия, Имя, Отчество"));
    this->_ledFullname = new QLineEdit(this->_grbSetting);
    this->_ledFullname->setGeometry(200, 30, 340, 20);

    this->_lblBirthday = new QLabel(this->_grbSetting);
    this->_lblBirthday->setGeometry(10, 70, 100, 20);
    this->_lblBirthday->setText(tr("Дата рождения"));
    this->_ledBirthday = new QLineEdit(this->_grbSetting);
    this->_ledBirthday->setGeometry(120, 70, 80, 20);

    this->_lblTestDateTime = new QLabel(this->_grbSetting);
    this->_lblTestDateTime->setGeometry(260, 70, 130, 20);
    this->_lblTestDateTime->setText(tr("Дата тестирования"));
    this->_ledTestDateTime = new QLineEdit(this->_grbSetting);
    this->_ledTestDateTime->setGeometry(400, 70, 140, 20);

    this->_lblTestType = new QLabel(this->_grbSetting);
    this->_lblTestType->setGeometry(10, 110, 200, 20);
    this->_lblTestType->setText(tr("Возраст тестирования"));
    this->_cmbTestType = new QComboBox(this->_grbSetting);
    this->_cmbTestType->setGeometry(220, 110, 220, 20);

    this->_lblOrganization = new QLabel(this->_grbSetting);
    this->_lblOrganization->setGeometry(10, 150, 200, 20);
    this->_lblOrganization->setText(tr("Тестирующая организация"));
    this->_cmbOrganization = new QComboBox(this->_grbSetting);
    this->_cmbOrganization->setGeometry(220, 150, 220, 20);
    this->_btnAddOrganization = new QPushButton(this->_grbSetting);
    this->_btnAddOrganization->setGeometry(450, 150, 90, 20);
    this->_btnAddOrganization->setText(tr("Добавить"));
    connect(this->_btnAddOrganization, SIGNAL(clicked()), this, SLOT(_slotHandleButtonAddOrganization()));

    this->_lblTestOperator = new QLabel(this->_grbSetting);
    this->_lblTestOperator->setGeometry(10, 190, 200, 20);
    this->_lblTestOperator->setText(tr("Оператор теста"));
    this->_cmbTestOperator = new QComboBox(this->_grbSetting);
    this->_cmbTestOperator->setGeometry(220, 190, 220, 20);
    this->_btnAddTestOperator = new QPushButton(this->_grbSetting);
    this->_btnAddTestOperator->setGeometry(450, 190, 90, 20);
    this->_btnAddTestOperator->setText(tr("Добавить"));
    connect(this->_btnAddTestOperator, SIGNAL(clicked()), this, SLOT(_slotHandleButtonAddTestOperator()));

    this->_grbCommand = new QGroupBox(this);
    this->_grbCommand->setGeometry(570, 10, 120, 220);

    this->_btnTestStart = new QPushButton(this->_grbCommand);
    this->_btnTestStart->setGeometry(10, 40, 100, 30);
    this->_btnTestStart->setText(tr("Начать тест"));
    connect(this->_btnTestStart, SIGNAL(clicked()), this, SLOT(_slotHandleButtonTestStart()));

    this->_btnCancel = new QPushButton(this->_grbCommand);
    this->_btnCancel->setGeometry(10, 100, 100, 30);
    this->_btnCancel->setText(tr("Отмена"));
    connect(this->_btnCancel, SIGNAL(clicked()), this, SLOT(_slotHandleButtonCancel()));
}

void SettingTestWidget::_SetComboboxListItems(QComboBox* cmb, QStringList items)
{
    cmb->clear();
    cmb->addItems(items);
}
/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */
void SettingTestWidget::_slotHandleButtonTestStart()
{

}

void SettingTestWidget::_slotHandleButtonCancel()
{

}

void SettingTestWidget::_slotHandleButtonAddOrganization()
{

}

void SettingTestWidget::_slotHandleButtonAddTestOperator()
{

}
/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */

