#include "reporttablewidget.h"

ReportTableWidget::ReportTableWidget()
{
    this->setWindowTitle(tr("Отчеты о тестировании"));
    this->setMinimumSize(640, 480);

    this->_fmtDate = "yyyy-MM-dd";
    this->_fmtTime = "hh:mm:ss";
    this->_datePeriod = new DatePeriodWidget();
    connect(this->_datePeriod, SIGNAL(HaveDatePeriod(QDate, bool)), this, SLOT(_slotHandleSignalDatePeriodHaveDate(QDate, bool)));

    this->_InitMainWidget();
}

ReportTableWidget::~ReportTableWidget()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void ReportTableWidget::_InitMainWidget()
{
    QHBoxLayout* hbox = new QHBoxLayout(this);
    this->_InitFilterWidget();
    hbox->addWidget(this->_wdFilter);

    this->_InitTableWidget();
    hbox->addWidget(this->_tableReport);
}

void ReportTableWidget::_InitFilterWidget()
{
    this->_wdFilter = new QWidget();
    this->_wdFilter->setFixedWidth(150);

    QVBoxLayout* vbox = new QVBoxLayout(this->_wdFilter);

    QPixmap _calendar("./Resource/calendar.png");
    _calendar.scaled(20, 20);
    QIcon _iconCalendar = QIcon(_calendar);

    this->_grbDateTimePeriod = new QGroupBox(this->_wdFilter);
    this->_grbDateTimePeriod->setTitle(tr("Период теста"));
    this->_grbDateTimePeriod->setGeometry(10, 10, 140, 200);
    this->_grbDateTimePeriod->setFixedHeight(200);

    this->_lblDatePeriod = new QLabel(this->_grbDateTimePeriod);
    this->_lblDatePeriod->setGeometry(10, 30, 120, 20);
    this->_lblDatePeriod->setText(tr("выберите даты"));

    this->_ledFirstDate = new QLineEdit(this->_grbDateTimePeriod);
    this->_ledFirstDate->setGeometry(10, 50, 90, 20);
    this->_btnFirstDate = new QPushButton(this->_grbDateTimePeriod);
    this->_btnFirstDate->setGeometry(100, 50, 20, 20);
    this->_btnFirstDate->setIcon(_iconCalendar);
    connect(this->_btnFirstDate, SIGNAL(clicked()), this, SLOT(_slotHandleButtonSelectFirstDate()));

    this->_btnLastDate = new QPushButton(this->_grbDateTimePeriod);
    this->_btnLastDate->setGeometry(10, 80, 20, 20);
    this->_btnLastDate->setIcon(_iconCalendar);
    connect(this->_btnLastDate, SIGNAL(clicked()), this, SLOT(_slotHandleButtonSelectLastDate()));
    this->_ledLastDate = new QLineEdit(this->_grbDateTimePeriod);
    this->_ledLastDate->setGeometry(30, 80, 90, 20);

    this->_lblTimePeriod = new QLabel(this->_grbDateTimePeriod);
    this->_lblTimePeriod->setGeometry(10, 120, 120, 20);
    this->_lblTimePeriod->setText(tr("Выберите время"));

    this->_ledFirstTime = new QTimeEdit(this->_grbDateTimePeriod);
    this->_ledFirstTime->setGeometry(10, 140, 120, 20);
    this->_ledFirstTime->setTime(QTime(8, 0, 0));
    this->_ledLastTime = new QTimeEdit(this->_grbDateTimePeriod);
    this->_ledLastTime->setGeometry(10, 170, 120, 20);
    this->_ledLastTime->setTime(QTime(18, 0, 0));

    vbox->addWidget(this->_grbDateTimePeriod);
    vbox->addStretch(5);

    this->_grbTestTypes = new QGroupBox(this->_wdFilter);
    this->_grbTestTypes->setTitle(tr("Тип теста"));
    this->_grbTestTypes->setGeometry(10, 250, 140, 150);
    this->_grbTestTypes->setFixedHeight(150);

    this->_chkTestTypes1 = new QCheckBox(this->_grbTestTypes);
    this->_chkTestTypes1->setGeometry(10, 30, 20, 20);
    this->_chkTestTypes1->setCheckState(Qt::Checked);
    this->_lblTestTypes1 = new QLabel(this->_grbTestTypes);
    this->_lblTestTypes1->setGeometry(35, 30, 100, 20);
    this->_lblTestTypes1->setText(tr("6.5 - 7 лет"));

    this->_chkTestTypes2 = new QCheckBox(this->_grbTestTypes);
    this->_chkTestTypes2->setGeometry(10, 70, 20, 20);
    this->_chkTestTypes2->setCheckState(Qt::Checked);
    this->_lblTestTypes2 = new QLabel(this->_grbTestTypes);
    this->_lblTestTypes2->setGeometry(35, 70, 100, 20);
    this->_lblTestTypes2->setText(tr("11 - 12 лет"));

    this->_chkTestTypes3 = new QCheckBox(this->_grbTestTypes);
    this->_chkTestTypes3->setGeometry(10, 110, 20, 20);
    this->_chkTestTypes3->setCheckState(Qt::Checked);
    this->_lblTestTypes3 = new QLabel(this->_grbTestTypes);
    this->_lblTestTypes3->setGeometry(35, 110, 100, 20);
    this->_lblTestTypes3->setText(tr("14 - 16 лет"));


    vbox->addWidget(this->_grbTestTypes);
    vbox->addStretch(5);

    this->_btnApplyFilter = new QPushButton(this->_wdFilter);
    this->_btnApplyFilter->setGeometry(30, 440, 100, 30);
    this->_btnApplyFilter->setText(tr("Применить"));
    connect(this->_btnApplyFilter, SIGNAL(clicked()), this, SLOT(_slotHandleButtonApplyFilter()));

    vbox->addWidget(this->_btnApplyFilter);
}

void ReportTableWidget::_InitTableWidget()
{
    this->_tableReport = new QTableWidget();
}
/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */
void ReportTableWidget::_slotHandleButtonApplyFilter()
{
    SettingReportFilter _settingReportFilter;
    QString _strFirstDate = this->_ledFirstDate->text();
    if (_strFirstDate != "")
    {
        _settingReportFilter.SetFirstDate(QDate::fromString(_strFirstDate, this->_fmtDate));
    }
    QString _strLastDate = this->_ledLastDate->text();
    if (_strLastDate != "")
    {
        _settingReportFilter.SetLastDate(QDate::fromString(_strLastDate, this->_fmtDate));
    }

    _settingReportFilter.SetFirstTime(this->_ledFirstTime->time());
    _settingReportFilter.SetLastTime(this->_ledLastTime->time());

    _settingReportFilter.SetTestTypes1(this->_chkTestTypes1->checkState() == Qt::Checked);
    _settingReportFilter.SetTestTypes2(this->_chkTestTypes2->checkState() == Qt::Checked);
    _settingReportFilter.SetTestTypes3(this->_chkTestTypes3->checkState() == Qt::Checked);
}

void ReportTableWidget::_slotHandleButtonSelectFirstDate()
{
    this->_datePeriod->SetIsFirstDate(true);
    this->_datePeriod->show();
}

void ReportTableWidget::_slotHandleButtonSelectLastDate()
{
    this->_datePeriod->SetIsFirstDate(false);
    this->_datePeriod->show();
}

void ReportTableWidget::_slotHandleSignalDatePeriodHaveDate(QDate date, bool flag)
{
    if (flag)
    {
        this->_ledFirstDate->setText(date.toString(this->_fmtDate));
    }
    else
    {
        this->_ledLastDate->setText(date.toString(this->_fmtDate));
    }
}
/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
