#include "aboutapplicationwidget.h"

AboutApplicationWidget::AboutApplicationWidget()
{
    this->setWindowTitle(tr("О приложении"));
    this->setFixedSize(500, 550);
    this->_InitWidget();
}

AboutApplicationWidget::~AboutApplicationWidget()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void AboutApplicationWidget::_InitWidget()
{
    this->_lblLogo = new QLabel(this);
    this->_lblLogo->setGeometry(5, 5, 73, 60);

    this->_lblVersion = new QLabel(this);
    this->_lblVersion->setGeometry(83, 5, 250, 30);
    this->_lblVersion->setAlignment(Qt::AlignLeft);

    this->_lblRequizite = new QTextBrowser(this);
    this->_lblRequizite->setGeometry(83, 40, 320, 110);
    this->_lblRequizite->setAlignment(Qt::AlignLeft);

    this->_btnOK = new QPushButton(this);
    this->_btnOK->setGeometry(420, 10, 70, 30);
    this->_btnOK->setText("OK");
    connect(this->_btnOK, SIGNAL(clicked()), this, SLOT(_slotHandleButtonOK()));

    this->_grbHistory = new QGroupBox(this);
    this->_grbHistory->setGeometry(5, 160, 490, 355);
    this->_grbHistory->setTitle("История изменений");

    this->_tbrHistory = new QTextBrowser(this->_grbHistory);
    this->_tbrHistory->setGeometry(5, 25, 480, 325);

}



/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */
void AboutApplicationWidget::_slotHandleButtonOK()
{
    this->close();
}
/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
