#ifndef ABOUTAPPLICATIONWIDGET_H
#define ABOUTAPPLICATIONWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPixmap>
#include <QTextBrowser>
#include <QPushButton>
#include <QGroupBox>
#include <QFile>
#include <QDomDocument>
#include <QStringList>

class AboutApplicationWidget : public QWidget
{
    Q_OBJECT
public:
    explicit AboutApplicationWidget();
    ~AboutApplicationWidget();

signals:

public:

public slots:

private:
    void _InitWidget();

private slots:
    void _slotHandleButtonOK();

private:
    QWidget* _wdAbout;
    QString _filename;

    QPushButton* _btnOK;
    QLabel* _lblVersion;
    QTextBrowser* _lblRequizite;
    QLabel* _lblLogo;
    QGroupBox* _grbHistory;
    QLabel* _lbHistory;
    QTextBrowser* _tbrHistory;

    QString _nameApplication;
    QString _authorApplication;
    QString _versionApplication;
    QString _mailAuthor;
    QString _siteAuthor;
    QString _addressAuthor;
    QStringList _phonesAuthor;

    QDomDocument doc;
};

#endif // ABOUTAPPLICATIONWIDGET_H
