#ifndef REPORTTABLEWIDGET_H
#define REPORTTABLEWIDGET_H

#include <QWidget>
#include <QTableWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QGroupBox>
#include <QDate>
#include <QTime>
#include <QTimeEdit>

#include "../../Tools/settingreportfilter.h"
#include "dateperiodwidget.h"

class ReportTableWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ReportTableWidget();
    ~ReportTableWidget();

signals:

public:

public slots:

private:
    void _InitMainWidget();
    void _InitTableWidget();
    void _InitFilterWidget();

private slots:
    void _slotHandleButtonApplyFilter();
    void _slotHandleButtonSelectFirstDate();
    void _slotHandleButtonSelectLastDate();

    void _slotHandleSignalDatePeriodHaveDate(QDate, bool);

private:
    QString _fmtDate;
    QString _fmtTime;

    DatePeriodWidget* _datePeriod;

    QTableWidget* _tableReport;
    QWidget* _wdFilter;

    QGroupBox* _grbDateTimePeriod;
    QLabel* _lblDatePeriod;
    QLineEdit* _ledFirstDate;
    QPushButton* _btnFirstDate;
    QLineEdit* _ledLastDate;
    QPushButton* _btnLastDate;
    QLabel* _lblTimePeriod;
    QTimeEdit* _ledFirstTime;
    QTimeEdit* _ledLastTime;

    QGroupBox* _grbTestTypes;
    QCheckBox* _chkTestTypes1;
    QLabel* _lblTestTypes1;
    QCheckBox* _chkTestTypes2;
    QLabel* _lblTestTypes2;
    QCheckBox* _chkTestTypes3;
    QLabel* _lblTestTypes3;

    QPushButton* _btnApplyFilter;
};

#endif // REPORTTABLEWIDGET_H
