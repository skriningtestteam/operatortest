#include "dateperiodwidget.h"

DatePeriodWidget::DatePeriodWidget()
{
    this->setFixedSize(480, 300);
    this->setWindowTitle(tr("Выберите дату"));

    this->_isFirstDate = true;
    this->_InitWidget();
}

DatePeriodWidget::~DatePeriodWidget()
{

}

/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */
bool DatePeriodWidget::IsFirstDate() const
{
    return _isFirstDate;
}

void DatePeriodWidget::SetIsFirstDate(bool isFirstDate)
{
    _isFirstDate = isFirstDate;
}
/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void DatePeriodWidget::_InitWidget()
{
    this->_calendar = new QCalendarWidget(this);
    this->_calendar->setGeometry(10, 10, 380, 280);

    this->_btnOK = new QPushButton(this);
    this->_btnOK->setGeometry(400, 40, 70, 40);
    this->_btnOK->setText(tr("OK"));
    connect(this->_btnOK, SIGNAL(clicked()), this, SLOT(_slotHandleButtonOK()));

    this->_btnCancel = new QPushButton(this);
    this->_btnCancel->setGeometry(400, 100, 70, 40);
    this->_btnCancel->setText(tr("Отмена"));
    connect(this->_btnCancel, SIGNAL(clicked()), this, SLOT(_slotHandleButtonCancel()));
}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */
void DatePeriodWidget::_slotHandleButtonOK()
{
    QDate _selectDate = this->_calendar->selectedDate();
    emit this->HaveDatePeriod(_selectDate, this->_isFirstDate);
    this->_calendar->setSelectedDate(QDate::currentDate());
    this->close();
}

void DatePeriodWidget::_slotHandleButtonCancel()
{
    this->close();
}


/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
