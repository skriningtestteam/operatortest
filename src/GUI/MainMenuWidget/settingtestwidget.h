#ifndef SETTINGTESTWIDGET_H
#define SETTINGTESTWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QGroupBox>
#include <QComboBox>
#include <QLineEdit>
#include <QDateTime>

class SettingTestWidget : public QWidget
{
    Q_OBJECT
public:
    explicit SettingTestWidget();
    ~SettingTestWidget();

signals:

public:
    void ShowWidget();
    void SetListTestType(QStringList);
    void SetListTestOrganization(QStringList);
    void SetListTestOperator(QStringList);

public slots:

private:
    void _InitWidget();
    void _SetComboboxListItems(QComboBox*, QStringList);

private slots:
    void _slotHandleButtonTestStart();
    void _slotHandleButtonCancel();
    void _slotHandleButtonAddOrganization();
    void _slotHandleButtonAddTestOperator();

private:
    QGroupBox* _grbSetting;
    QGroupBox* _grbCommand;

    QLabel* _lblFullname;
    QLineEdit* _ledFullname;
    QLabel* _lblBirthday;
    QLineEdit* _ledBirthday;
    QLabel* _lblTestDateTime;
    QLineEdit* _ledTestDateTime;

    QLabel* _lblTestType;
    QComboBox* _cmbTestType;
    QLabel* _lblOrganization;
    QComboBox* _cmbOrganization;
    QPushButton* _btnAddOrganization;
    QLabel* _lblTestOperator;
    QComboBox* _cmbTestOperator;
    QPushButton* _btnAddTestOperator;

    QPushButton* _btnTestStart;
    QPushButton* _btnCancel;
};

#endif // SETTINGTESTWIDGET_H
