#ifndef DATEPERIODWIDGET_H
#define DATEPERIODWIDGET_H

#include <QWidget>
#include <QCalendarWidget>
#include <QPushButton>

class DatePeriodWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DatePeriodWidget();
    ~DatePeriodWidget();

signals:
    void HaveDatePeriod(QDate, bool);

public:
    bool IsFirstDate() const;
    void SetIsFirstDate(bool IsFirstDate);

public slots:

private:
    void _InitWidget();

private slots:
    void _slotHandleButtonOK();
    void _slotHandleButtonCancel();

private:
    bool _isFirstDate;

    QCalendarWidget* _calendar;
    QPushButton* _btnOK;
    QPushButton* _btnCancel;
};

#endif // DATEPERIODWIDGET_H
