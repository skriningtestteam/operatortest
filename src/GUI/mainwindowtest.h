#ifndef MAINWINDOWTEST_H
#define MAINWINDOWTEST_H

#include <QMainWindow>
#include <QMenuBar>

#include "../Tools/maincontoller.h"
#include "MainMenuWidget/settingapplicationwidget.h"
#include "MainMenuWidget/settingtestwidget.h"
#include "MainMenuWidget/reporttablewidget.h"
#include "MainMenuWidget/aboutapplicationwidget.h"

class MainWindowTest : public QMainWindow
{
    Q_OBJECT

public:
    MainWindowTest(MainContoller*);
    ~MainWindowTest();

public:

public slots:

private:
    void _CreateMainMenu();

private slots:
    void _slotShowSettingApplication();
    void _slotShowSettingTest();
    void _slotShowTableReports();
    void _slotShowAboutApplication();
    void _slotShowHelp();

private:
    QMenuBar * _mainMenu;
    QMenu * _settingMenu;
    QMenu * _reportMenu;
    QMenu * _aboutMenu;

    MainContoller* _mainController;
    SettingApplicationWidget* _settingApplication;
    SettingTestWidget* _settingTest;
    ReportTableWidget* _tableReport;
    AboutApplicationWidget* _aboutApplication;
};

#endif // at
