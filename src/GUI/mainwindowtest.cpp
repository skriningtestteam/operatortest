#include "mainwindowtest.h"

MainWindowTest::MainWindowTest(MainContoller* controller):
    _mainController(controller)
{
    this->setWindowTitle("Оператор теста");
    this->setMinimumHeight(600);
    this->setMinimumWidth(800);

    this->_CreateMainMenu();
}

MainWindowTest::~MainWindowTest()
{
    delete this->_settingMenu;
    delete this->_reportMenu;
    delete this->_aboutMenu;
    delete this->_mainMenu;
}


/*
 * Public Method's
 ______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Method's
 */

/*
 * Public Slot's
 _______________________________________________________________________________________________________________________
 */

/*
 ______________________________________________________________________________________________________________________
 * End Public Slot's
 */

/*
 * Private Method's
 ______________________________________________________________________________________________________________________
 */
void MainWindowTest::_CreateMainMenu()
{
    this->_mainMenu = new QMenuBar(this);
    this->_mainMenu->setMinimumWidth(340);

    this->_settingMenu = new QMenu(tr("Настройка"));
    this->_settingMenu->addAction(tr("Настройка приложения"), this, SLOT(_slotShowSettingApplication()), Qt::CTRL + Qt::Key_S);
    this->_settingMenu->addAction(tr("Настройка теста"), this, SLOT(_slotShowSettingTest()), Qt::CTRL + Qt::Key_T);
    this->_mainMenu->addMenu(this->_settingMenu);

    this->_reportMenu = new QMenu(tr("Отчеты"));
    this->_reportMenu->addAction(tr("Отчеты о тестировании"), this, SLOT(_slotShowTableReports()), Qt::CTRL + Qt::Key_R);
    this->_mainMenu->addMenu(this->_reportMenu);

    this->_aboutMenu = new QMenu(tr("О приложении"));
    this->_aboutMenu->addAction(tr("Информация о приложении"), this, SLOT(_slotShowAboutApplication()), Qt::CTRL + Qt::Key_Q);
    this->_mainMenu->addMenu(this->_aboutMenu);

    this->_mainMenu->show();

    this->_settingApplication = new SettingApplicationWidget(this->_mainController->GetSettingApplication());
    this->_settingTest = new SettingTestWidget();
    this->_tableReport = new ReportTableWidget();
    this->_aboutApplication = new AboutApplicationWidget();
}

/*
 ______________________________________________________________________________________________________________________
 * End Private Method's
 */

/*
 * Private Slot's
 _______________________________________________________________________________________________________________________
 */
void MainWindowTest::_slotShowSettingApplication()
{
    this->_settingApplication->show();
}

void MainWindowTest::_slotShowSettingTest()
{
    this->_settingTest->ShowWidget();
}

void MainWindowTest::_slotShowTableReports()
{
    this->_tableReport->show();
}

void MainWindowTest::_slotShowAboutApplication()
{
    this->_aboutApplication->show();
}

void MainWindowTest::_slotShowHelp()
{

}
/*
 ______________________________________________________________________________________________________________________
 * End Private Slot's
 */
